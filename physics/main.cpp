#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include <iostream>
#include <cmath>
#define PI 3.14159265
using namespace std;

double G = 6.67 * pow(10, -11);
double M = 1.99 * pow(10, 30);
double rho = 1.496 * pow(10, 11);
double borders = 100.0;
double x = 1, y = 0;
double v_x = 0, v_y = 0;
double dt = 0.005;
double angle = 0, v_0 = 0;
double size_change = pow((M * G / rho), 0.5);
double t = dt;

double velocity(double v, double x_y, double y_x, double ta){
     return (v - (x_y / pow((sqrt(pow(x_y, 2) + pow(y_x, 2))), 3) * ta));
}

void get_values(){
    cout << "Enter angle (deg): ";
    cin >> angle;
    cout << "Enter u_0 (m/c): ";
    cin >> v_0;
    cout << "Enter x_0 (a.e): ";
    cin >> x;
}

void change_size(GLsizei w, GLsizei h){
    GLdouble aspect_ratio;
    if (h == 0) h = 1;
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
     aspect_ratio = (GLdouble)w / (GLdouble)h;
     if (w <= h)
         glOrtho(-borders, borders, -borders / aspect_ratio, borders / aspect_ratio, -1.0, 1.0);
     else
         glOrtho(-borders * aspect_ratio, borders * aspect_ratio, -borders, borders, -1.0, 1.0);
     glMatrixMode(GL_MODELVIEW);
     glLoadIdentity();
}

void initialise(){
     glClearColor(0, 0, 0, 0);
}

void experiment(){
    cout << "Experiment: " << endl;
    cout << "Enter x (a.e.): ";
    cin >> x;
    double v_1 = pow(1 / x, 0.5);
    double v_2 = v_1 * pow(2, 0.5);
    for(v_y = v_1; v_y < INT_MAX; v_y++){
        if(v_y / size_change >= v_2) break;
    }
    cout << "Ellipse: v < " << v_y << endl;
    cout << "Parabola: v = " << v_y << endl;
    cout << "Hyperbola: v > " << v_y << endl;
}

void render_scene_2(){
    double new_dt = 0.2;
    double new_t = dt, new_x = x, new_y = y, new_v_x = v_x, new_v_y = v_y;
    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_POINTS);
    while (new_t <= 100){
        glVertex2d(new_x, new_y);
        new_v_x = velocity(new_v_x, new_x, new_y, new_dt);
        new_v_y = velocity(new_v_y, new_y, new_x, new_dt);
        new_x += new_v_x * new_dt;
        new_y += new_v_y * new_dt;
        new_t += new_dt;
    }
    glEnd();
    glFlush();
}

void render_scene(){
    glClear(GL_COLOR_BUFFER_BIT);
    glColor3f(1.0, 0.0, 0.0);
    glBegin(GL_LINES);
    glVertex2f(-borders, 0.0);
    glVertex2f(borders, 0.0);
    glEnd();
    glBegin(GL_LINES);
    glVertex2f(0.0, -borders);
    glVertex2f(0.0, borders);
    glEnd();
    
    glColor3f(0.0, 1.0, 0.0);
    double radian = 0.0;
    double x_new, y_new;
    glBegin(GL_POINTS);
    while (radian <= 2 * PI){
         x_new = cos(radian);
         y_new = sin(radian);
         glVertex2d(x_new, y_new);
         radian += 0.005;
    }
    glEnd();
    
    render_scene_2();
    glColor3f(0.0, 0.0, 1.0);
    glBegin(GL_POINTS);
    while (t <= 10000){
        glVertex2d(x, y);
        v_x = velocity(v_x, x, y, dt);
        v_y = velocity(v_y, y, x, dt);
        x += v_x * dt;
        y += v_y * dt;
        t += dt;
    }
    glEnd();
    glFlush();
    
}

int main(int argc, char **argv){
    experiment();
    get_values();
    v_x = (v_0 * cos(angle * PI / 180)) / (size_change);
    v_y = (v_0 * sin(angle * PI / 180)) / (size_change);
    borders = max(x, y) * 10;
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(640, 640);
    glutInitWindowPosition(20, 20);
    glutCreateWindow("Physics");
    glutDisplayFunc(render_scene);
    glutReshapeFunc(change_size);
    initialise();
    glutMainLoop();
    return 0;
}
